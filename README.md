# Poke Berries Statistics API :bar_chart:

 _Backend project developed in Python following Globant's challenge specifications_\
The API allows you to get statistics about berries.\
Berries are small fruits that can provide HP and status condition restoration, stat enhancement, and even damage negation when eaten by Pokémon.

# First things first 📋

Before cloning the repository, it's recommended to create a virtual environment. That will create an isolated environment for the project. This means that the project can have its own dependencies, regardless of what dependencies every other project in your system has.

This is where you are going to install the requirements that make the project works correctly.

To do that, you only have to run these commands in the Terminal window:
```
python3 -m venv env                     # this create the environment
source env/bin/activate                  # this will activated the environment
```

# Clone the repository 📦

You can do it by following this command:

```
git clone https://gitlab.com/paula.giammarini/poke-berries-statistics.git
```
After that, install the requirements:

```
cd poke-berries-statistics
pip install -r requirements.txt      # this will install all you need for the project
cd berries_statistics/
```

I recommend you not to deactivate the environment for now, as you will need it for the following steps.
```
deactivate                           # this will deactivated the environment in case you want to.
```


# Environment configuration :wrench:

For everything to work correctly, it is necessary to set some variables in our environment.
In order to do that, run these commands on a Terminal window:

```
export DJANGO_SETTINGS_MODULE=berries_statistics.settings
export DEBUG=TRUE 
export SECRET_KEY=<***>    # The <***> has to be replaced by the correct key.
```
__Note:__ It's important not to let any blank spaces between = operator.

After that, you have to run migrations:

```
python manage.py migrate
```


# Ready to run :arrow_forward:

Make sure you have you enviroment activated and you are in /poke-berries-statistics/berries_statistics/

Here you have 2 ways to test the project:

1) The first one is by turning on the server and using a browser.\
For that run this commands on a Terminal window:

```
python manage.py runserver
```

Now you are able to go to a browser and enter to this URL http://localhost:8000/api/report/ \
__This way you make a GET request to the only endpoint the API has. No params are needed, if you send some will be ignored.__\
(You also could do this by using an external tool like Postman if you want)

Here you will see the statistics calculated from the poke-berries with a histogram which represents their frecuency of the growth times.


2) The other way to test the project is by running the developed test.\
If you want to run them use the following command on Terminal window:

```
pytest report/tests --verbose
```
Other way to check them is by using coverage:

```
pytest --cov-report term-missing  --cov=. report/tests

```

That's all for now. I hope you enjoyed it!
