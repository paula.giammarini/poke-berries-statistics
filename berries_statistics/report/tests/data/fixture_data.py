successful_request = {
    "count": 5,
    "next": None,
    "previous": None,
    "results": [
        {"name": "cheri", "url": "https://pokeapi.co/api/v2/berry/1/"},
        {"name": "chesto", "url": "https://pokeapi.co/api/v2/berry/2/"},
        {"name": "pecha", "url": "https://pokeapi.co/api/v2/berry/3/"},
        {"name": "rawst", "url": "https://pokeapi.co/api/v2/berry/4/"},
        {"name": "aspear", "url": "https://pokeapi.co/api/v2/berry/5/"},
    ],
}

names = ['cheri', 'chesto', 'pecha', 'rawst', 'aspear'] 
         
growth_times = [1, 1, 3, 5, 5]

report = {
    'berries_names': names, 
    'min_growth_time': 1,
    'median_growth_time': 3,
    'max_growth_time': 5,
    'variance_growth_time': 3.4,
    'mean_growth_time': 5.0,
    'frequency_growth_time': {1: 5, 3: 25, 4: 15, 5: 20, 8: 5}
}

empty_report = {
    'berries_names': [],
    'min_growth_time': 0,
    'median_growth_time': 0.0,
    'max_growth_time': 0,
    'variance_growth_time': 0.0,
    'mean_growth_time': 0.0,
    'frequency_growth_time': {}
}