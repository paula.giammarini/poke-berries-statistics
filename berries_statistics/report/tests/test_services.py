import aiohttp
import json
from report.services import ReportServices
from report.tests.data.fixture_data import (
    empty_report,
    names,
    successful_request,
)


def _mocker_response(mocker, status=200, content="CONTENT",
                     json_data=None, raise_for_status=None):
        mocker_resp = mocker.Mock()
        mocker_resp.raise_for_status = mocker.Mock()
        if raise_for_status:
            mocker_resp.raise_for_status.side_effect = raise_for_status
        mocker_resp.status_code = status
        mocker_resp.content = content
        if json_data:
            mocker_resp.json = mocker.Mock(
                return_value=json_data
            )
        return mocker_resp

def test_make_request_ok(mocker):
    mocker_resp = _mocker_response(mocker, content=json.dumps(successful_request))
    mocker.patch('requests.request', return_value = mocker_resp)
    result = ReportServices.make_request()
    assert result == successful_request

def test_process_data_ok(mocker):
    mock = aiohttp.ClientSession
    mock.get = mocker.MagicMock()
    mock.get.return_value.__aenter__.return_value.status = 200
    mock.get.return_value.__aenter__.return_value.json.return_value = {'growth_time': 1}
    result_names, result_growth_times = ReportServices.process_data(
        successful_request['results']
    )
    assert result_names == names
    assert result_growth_times == [1, 1, 1, 1, 1]

def test_make_report_ok(mocker):
    expected = {
        'berries_names': ['cheri', 'chesto', 'pecha', 'rawst'],
        'min_growth_time': 2,
        'median_growth_time': 2.5,
        'max_growth_time': 3,
        'variance_growth_time': 3.0,
        'mean_growth_time': 2.5,
        'frequency_growth_time': {1:2, 3:2}
    }
    mocker.patch(
        'report.arithmetic_functions.Calculator.calculate_variance',
        return_value = 3.0
    )
    mocker.patch(
        'report.arithmetic_functions.Calculator.calculate_median',
        return_value = 2.5
    )
    mocker.patch(
        'report.arithmetic_functions.Calculator.calculate_mean',
        return_value = 2.5
    )
    mocker.patch(
        'report.arithmetic_functions.Calculator.calculate_min',
        return_value = 2
    )
    mocker.patch(
        'report.arithmetic_functions.Calculator.calculate_max',
        return_value = 3
    )
    mocker.patch(
        'report.arithmetic_functions.Calculator.calculate_frecuency',
        return_value = {1:2, 3:2}
    )
    result = ReportServices.make_report(
        ['cheri', 'chesto', 'pecha', 'rawst'],
        [1, 1, 3, 3]
    )
    assert result == expected

def test_make_report_empty():
    result = ReportServices.make_report([], [])
    assert result == empty_report
