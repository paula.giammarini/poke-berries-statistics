from report.views import ReportViewSet
from report.tests.data.fixture_data import (
    empty_report,
    growth_times,
    names,
    report,
    successful_request,
)

def test_list_ok(mocker):
    mocker.patch(
        'report.services.ReportServices.make_request',
        return_value = successful_request
    )
    mocker.patch(
        'report.services.ReportServices.process_data',
        return_value = [names, growth_times]
    )
    mocker.patch(
        'report.services.ReportServices.make_report',
        return_value = report
    )
    result = ReportViewSet().list(None)
    assert result.status_code == 200
    assert result.data == report

def test_list_ok_empty(mocker):
    mocker.patch('report.services.ReportServices.make_request', return_value = {})
    mocker.patch('report.services.ReportServices.process_data', return_value = [])
    mocker.patch('report.services.ReportServices.make_report', return_value = empty_report)
    result = ReportViewSet().list(None)
    assert result.status_code == 200
    assert result.data == empty_report