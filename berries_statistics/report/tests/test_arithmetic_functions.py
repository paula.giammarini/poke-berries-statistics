from report.arithmetic_functions import Calculator as cal


values_list = [1, 4, 5, 2, 6, 1, 1, 2, 3, 5]


def test_frecuency():
    result = cal.calculate_frecuency(values_list)
    assert result == {1: 3, 2: 2, 3: 1, 4: 1, 5: 2, 6: 1}

def test_minimun():
    result = cal.calculate_min(values_list)
    assert result == 1

def test_maximun():
    result = cal.calculate_max(values_list)
    assert result == 6

def test_mean():
    result = cal.calculate_mean(values_list)
    assert result == 3

def test_median():
    result = cal.calculate_median(values_list)
    assert result == 2.5

def test_variance():
    result = cal.calculate_variance(values_list)
    assert result == 3.56