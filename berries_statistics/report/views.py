from rest_framework import (
    status,
    viewsets,
)
from rest_framework.permissions import AllowAny
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response

from report.services import ReportServices


class ReportViewSet(viewsets.GenericViewSet):
    permission_classes = [AllowAny]
    http_method_names = ['get']
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'histogram.html'

    def list(self, request):
        ReportServices.clean_old_file()
        all_berries = ReportServices.make_request()
        names = []
        growth_times = []
        if 'results' in all_berries:
            names, growth_times = ReportServices.process_data(
                all_berries['results']
            )
        response = ReportServices.make_report(names, growth_times)
        ReportServices.make_graphic(growth_times)
        return Response(
            response,
            status=status.HTTP_200_OK
        )