from django.urls import include, path

from rest_framework import routers

from report.views import ReportViewSet


app_name = 'api'

router = routers.SimpleRouter()

router.register(r'report', ReportViewSet, basename='report')


urlpatterns = [
    path(r'api/', include(router.urls)),
]
