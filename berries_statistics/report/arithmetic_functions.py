from statistics import mean, median, variance


class Calculator:

    @staticmethod
    def calculate_frecuency(values_list):
        frecuency = {}
        for value in set(values_list):
            frecuency[value] = values_list.count(value)
        return frecuency

    @staticmethod
    def calculate_min(values_list):
        return min(values_list)

    @staticmethod
    def calculate_max(values_list):
        return max(values_list)

    @staticmethod
    def calculate_median(values_list):
        return round(median(values_list), 2)

    @staticmethod
    def calculate_variance(values_list):
        return round(variance(values_list), 2)

    @staticmethod
    def calculate_mean(values_list):
        return round(mean(values_list), 2)