import asyncio
import json
import requests
import os
import matplotlib.pyplot as plt

from report.arithmetic_functions import Calculator as cal
from report.async_services import AsyncRequestManager


class ReportServices:

    @staticmethod
    def clean_old_file():
        path = 'static/histogram.png'
        if os.path.exists(path):
            os.remove(path)

    @staticmethod
    def make_request():
        response = requests.request(
            "GET",
            'https://pokeapi.co/api/v2/berry/',
            params={'offset': 0, 'limit': 999999},
        )
        response.raise_for_status()
        result_dict = json.loads((response.content))
        return result_dict
    
    @classmethod
    def process_data(cls, berries):
        growth_times = []
        names = [ x['name'] for x in berries ]
        growth_times = asyncio.run(
            AsyncRequestManager.get_multiples_requests(berries)
        )
        return names, growth_times

    @classmethod
    def make_report(cls, names, growth_times):
        if names and growth_times:
            response = {
                'berries_names': names,
                'min_growth_time': cal.calculate_min(growth_times),
                'median_growth_time': cal.calculate_median(growth_times),
                'max_growth_time': cal.calculate_max(growth_times),
                'variance_growth_time': cal.calculate_variance(growth_times),
                'mean_growth_time': cal.calculate_mean(growth_times),
                'frequency_growth_time': cal.calculate_frecuency(growth_times)
            }
        else:
            response = {
                'berries_names': [],
                'min_growth_time': 0,
                'median_growth_time': 0.0,
                'max_growth_time': 0,
                'variance_growth_time': 0.0,
                'mean_growth_time': 0.0,
                'frequency_growth_time': {}
            }
        return response

    @staticmethod
    def make_graphic(growth_times):
        plt.hist(growth_times, bins=30, alpha=1,
                 edgecolor = 'black', linewidth=1, color='orange')
        if growth_times:
            plt.xticks(range(max(growth_times) + 2))
        plt.xlabel("Values")
        plt.ylabel("Frequency")
        plt.title("Growth Times Histogram")
        plt.savefig("static/histogram.png")
