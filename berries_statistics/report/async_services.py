import aiohttp
import asyncio


class AsyncRequestManager:

    @staticmethod
    async def get_berry_request(session, berry):
        async with session.get(berry['url']) as response:
            result_data = await response.json()
            results = result_data['growth_time']
            return results

    @classmethod
    async def get_multiples_requests(cls, berries):
        growth_times = []
        async with aiohttp.ClientSession() as session:
            tasks = []
            for berry in berries:
                task = asyncio.ensure_future(
                    cls.get_berry_request(session, berry)
                )
                tasks.append(task)
            growth_times = await asyncio.gather(*tasks)
        return growth_times
